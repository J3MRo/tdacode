import { ROUTE_HOME, ROUTE_STATISTICS, ROUTE_PLAYERS } from '../routes';

const sidebarLinks = [
    {
        path: ROUTE_HOME,
        name: "Home",
        id: 'home',
    },
    {
        path: ROUTE_STATISTICS,
        name: "Estadisticas",
        id: "statistics",
    },
    {
        path: ROUTE_PLAYERS,
        name: "Jugadores",
        id: "players",
    }
];

export { sidebarLinks }