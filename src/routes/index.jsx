import React from 'react'
import Home from "../layouts/Home";
import Statistics from "../layouts/Statistics";
import Players from "../layouts/Players";
import { ROUTE_HOME, ROUTE_STATISTICS, ROUTE_PLAYERS } from './routes';

export const indexRoutes = [
    { path: ROUTE_HOME, name: 'Home', component: (props) => <Home {...props} /> },
    { path: ROUTE_STATISTICS, name: 'Statistics', component: (props) => <Statistics {...props} />},{ path: ROUTE_PLAYERS, name: 'Players', component: (props) => <Players {...props} />}
];

