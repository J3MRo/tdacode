import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import { indexRoutes } from "./routes";


function App() {
  return (
    <div className="App">
      
      <BrowserRouter>
        <Switch>
        
        {indexRoutes.map((props, key) => {
                                return <Route exact path={props.path} component={props.component} key={key} />; 
                        })}
        </Switch>
      </BrowserRouter>    
    </div>
  );
}

export default App;
