import React, { Component, Fragment} from 'react'
import Sidebar from "../components/Sidebar";
import venados_shield from "../assets/sidebar/vedos_shield.png";
// import { fakegames } from "../fakedata/games";

class Home extends Component {
    // state = { 
    //     error: null,
    //     isLoaded: false,
    //     items: [],
    //     value:[]
    //  }

    //  componentDidMount() {
    //     fetch("https://venados.dacodes.mx/api/games")
    //       .then(res => res.json())
    //       .then(
    //         (data) => {
    //             console.log(data);        
    //           this.setState({
    //             value: data
    //           });
    //         })
    //         .catch(console.log)
    //   }

    render() { 
        return ( 
            <Fragment>
                <Sidebar />
                <div id="Home" className="container-fluid">
                    <div className="home-img-container">
                        <img src={ venados_shield} alt="Escudo Venados" />
                    </div>
                    <div className="home-container-buttons">
                    <button type="button" className="btn btn-outline-dark">COPA MX</button>
                    <button type="button" className="btn btn-outline-dark">ASCENSO MX</button>
                    </div>
                    <div className="home-container-games">
            {/** This info are hard-core if whe have a API we need to remove this and use a map */}
                        <div className="home-games-month">Agosto</div>
        
                        <div className="row">
                            <div className="col  home-games-listday">
                                <ul>
                                    <li>SAB</li>
                                    <li>16</li>
                                </ul>
                            </div>
                            <div className="col home-games-shield-one">
                                <img src={venados_shield} alt="team-shield1"/>
                            </div>
                            <div className="col home-games-score">
                                <span>2-1</span>
                            </div>
                            <div className="col home-game-shield-two">
                            <img src={venados_shield} alt="team-shield2"/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col home-games-listday">
                                <ul>
                                    <li>SAB</li>
                                    <li>16</li>
                                </ul>
                            </div>
                            <div className="col home-games-shield-one">
                                <img src={venados_shield} alt="team-shield1"/>
                            </div>
                            <div className="col home-games-score">
                                <span>2-1</span>
                            </div>
                            <div className="col home game-shield-two">
                            <img src={venados_shield} alt="team-shield2"/>
                            </div>
                        </div>
             {/** This info are hard-core if whe have a API we need to remove this and use a map */}
                    </div>
                </div>
            </Fragment>
         );
    }
}
 
export default Home;