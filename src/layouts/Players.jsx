import React, { Component, Fragment} from 'react'
import { fakeplayerinfo } from '../fakedata/players/index';
import Sidebar from "../components/Sidebar";
import Modal from "../components/Modal";

class Players extends Component {

    state = { show: false };

    showModal = () => {
        console.log("holassad");
        
        this.setState({ show: true });
    };

    hideModal = () => {
        this.setState({ show: false });
    };

    render() { 
        return ( 
            <Fragment>
                <Sidebar/>
                <div id="Players" className="container-fluid">
                    <div className="players-container-label">
                        <h5>Jugadores</h5>
                    </div>
                    <div className="players-container-players-list">
                        <div className="row">
                            {
                                fakeplayerinfo.map((prop, key) => {
                                    return (
                                        <div key={key} className="col-4" onClick={this.showModal}>
                                            <ul>
                                                <li>
                                                <img src={prop.photo} className="rounded-circle" alt="photo_player"/>
                                                </li>
                                                <li>{prop.position}</li>
                                                <li>{prop.player}</li>
                                            </ul>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
                <Modal show={this.state.show} handleClose={this.hideModal}>
                <p>Modal</p>
                <p>Data</p>
                </Modal>
            </Fragment>
         );
    }
}
 
export default Players;