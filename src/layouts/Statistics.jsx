import React, { Component, Fragment} from 'react'

import Sidebar from "../components/Sidebar";
import venados_shield from "../assets/sidebar/vedos_shield.png";

class Statistics extends Component {
    state = {  }
    render() { 
        return ( 
            <Fragment>
                <Sidebar />
                <div id="Statistics" className="container-fluid">
                    
                        <div className="row statistics-title-container">
                            <div className="col-6">Tabla General</div>
                            <div className="col-2">JJ</div>
                            <div className="col-2">DG</div>
                            <div className="col-2">PTS</div>
                        </div>
                        <div className="row statistics-info-container">
                            <div className="col-2 statistics-info-num">1</div>
                            <div className="col-4 statistics-info-team">
                            <img src={venados_shield} alt="team-shield"/>
                            <span>Venados F.C</span>
                            </div>
                            <div className="col-2">2</div>
                            <div className="col-2">5</div>
                            <div className="col-2">6</div>
                        </div>
                    
                </div>
            </Fragment>
         );
    }
}
 
export default Statistics;