/* eslint-disable no-self-compare */
import React, { Component} from 'react'
import Aux from '../hoc/Aux'
import { Link } from "react-router-dom";
import venados_shield from "../assets/sidebar/vedos_shield.png";
import {sidebarLinks} from "../routes/sidebar";

class Sidebar extends Component {

  rendersidebarLinks = () => {
    return (
      sidebarLinks.map((prop, key) => {
            let route = prop.path
            route = route.replace(':projectId', this.props.projectId)
            return (
                <li key={key} className="sidebar-item active">
                    <Link to={route} className="sidebar-anchor">
                        {prop.name}
                    </Link>
                </li>
            )
        })
    )
  }

  state = { showMenu: false }

  toggleMenu = () => {
    this.setState({
      showMenu: !this.state.showMenu
      
    })
  }

    render() {
      const menuActive = this.state.showMenu ? 'active' : ''; 
      const menuOut = this.state.showMenu ? 'move-to-right' : ''; 
        return ( 
          
            <Aux>
              <div id="main-sidebar">

              {/* <div className="nav-right" onClick={this.toggleMenu}>
                <div className="button" id="btn">
                  <div className="bar top"></div>
                  <div className="bar middle"></div>
                  <div className="bar bottom"></div>
                </div>
              </div> */}

              <div className="nav-right" onClick={this.toggleMenu} >
                    <div className={"button " + menuActive} id="btn"> {/*active*/}
                      <div className="bar top "></div>
                      <div className="bar middle "></div>
                      <div className="bar bottom "></div>
                    </div>
                  </div>

              <div className={"sidebar " + menuOut}>{/*move-to-right*/}
                <div className="top-sidebar">

                  <div className="sidebar-distance">
                  <div className="sidebar-img-circle">
                  <img src={venados_shield}  alt="Escudo Venados" /> 
                  </div>
                  </div>
                </div>
                <ul className="sidebar-list">
                  {this.rendersidebarLinks()}
                </ul>
              </div>

              </div>
               
            </Aux>
         );
    }
}
 
export default Sidebar;

