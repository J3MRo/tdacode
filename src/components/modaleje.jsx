import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types';
import { defaultProps } from 'recompose';


class Modal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: this.props.show
        }
    }

    toggle = () => {
        this.setState({
            show: false
        })
    }

    onClickConfirm = () => {
        console.log("confirm")
        this.toggle()
    }

    onClickReject = () => {
        this.toggle()
    }

    componentWillReceiveProps({ show }) {
        this.setState({
            show
        })
    }

    closeModal = (e) => {
        e.preventDefault()
        this.props.closeModal()
    }

    render() {
        return (
            <Fragment>

                {/* The Modal */}
            <div class="modal fade" id="myModal">
                <div class="modal-dialog">
                <div class="modal-content">
                
                    {/* Modal Header */}
                    <div class="modal-header">
                    <h4 class="modal-title">Modal Heading</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    
                    {/* Modal body */}
                    <div class="modal-body">
                    Modal body..
                    </div>
                    
                    {/* Modal footer  */}
                    <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                    
                </div>
                </div>
            </div>
  
                
            </Fragment>
        )
    }
}




export default Modal;