import calendar_white from "../../assets/calendar_Icon_White.svg"
import venados_shield from "../../assets/sidebar/vedos_shield.png"

const fakegames = [
    {
        month:"AGOSTO",
        week_scores:[
            {
                week_day:"SAB",
                day:16,
                teams:[{
                    id:"team1",
                    name:"Venados",
                    score:2,
                    team_icon:venados_shield
                },
                {
                    id:"team2",
                    name:"Venados Dos",
                    score:1,
                    team_icon:venados_shield
                }
            ],
            },

            {
                week_day:"DOM",
                day:17,
                teams:[{
                    id:"team3",
                    name:"Venados Tres",
                    score:1,
                    team_icon:venados_shield
                },
                {
                    id:"team4",
                    name:"Venados Cuatro",
                    score:3,
                    team_icon:venados_shield
                }
            ],
            },

        ],
    icon:calendar_white
    },

    {
        month:"SEPTIEMBRE",
        week_scores:[
            {
                week_day:"SAB",
                day:20,
                teams:[{
                    id:"team5",
                    name:"Venados Cinco",
                    score:2,
                    team_icon:venados_shield
                },
                {
                    id:"team6",
                    name:"Venados Seis",
                    score:1,
                    team_icon:venados_shield
                }
            ],
            },

            {
                week_day:"DOM",
                day:21,
                teams:[{
                    id:"team7",
                    name:"Venados Siete",
                    score:1,
                    team_icon:venados_shield
                },
                {
                    id:"team8",
                    name:"Venados Ocho",
                    score:3,
                    team_icon:venados_shield
                }
            ],
            },

        ],
    icon:calendar_white
    }

];

export { fakegames }